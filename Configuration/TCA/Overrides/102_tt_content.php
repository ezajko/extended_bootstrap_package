<?php
defined('TYPO3_MODE') || die();

// Add new EXT:container CTypes

// GRID COLUMNS
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
    new \B13\Container\Tca\ContainerConfiguration(
        '2_col',
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.2Columns.title',
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.2Columns.description',
        [
            [
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col1', 'colPos' => 101],
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col2', 'colPos' => 102],
            ],
        ]
    )
    )
        ->setIcon('EXT:container/Resources/Public/Icons/container-2col.svg')
        ->setBackendTemplate('EXT:extended_bootstrap_package/Resources/Private/Backend/Templates/Container/Columns.html')
        ->setGroup('Grids')
);
$GLOBALS['TCA']['tt_content']['types']['2_col']['showitem'] = '
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
			 --div--;LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:columns.options, pi_flexform;LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:advanced,
		--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
			--palette--;;language,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
			--palette--;;hidden,
			--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
			categories,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
			rowDescription,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended
';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:extended_bootstrap_package/Configuration/FlexForms/Container/2Columns.xml',
    '2_col'
);

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
    new \B13\Container\Tca\ContainerConfiguration(
        '3_col', // CType
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.3Columns.title',
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.3Columns.description',
        [
            [
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col1', 'colPos' => 101],
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col2', 'colPos' => 102],
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col3', 'colPos' => 103],
            ],
        ]
    )
    )
        ->setIcon('EXT:container/Resources/Public/Icons/container-3col.svg')
        ->setBackendTemplate('EXT:extended_bootstrap_package/Resources/Private/Backend/Templates/Container/Columns.html')
);
$GLOBALS['TCA']['tt_content']['types']['3_col']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['2_col']['showitem'];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:extended_bootstrap_package/Configuration/FlexForms/Container/3Columns.xml',
    '3_col'
);
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
    new \B13\Container\Tca\ContainerConfiguration(
        '4_col',
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.4Columns.title',
        'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.4Columns.description',
        [
            [
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col1', 'colPos' => 101],
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col2', 'colPos' => 102],
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col3', 'colPos' => 103],
                ['name' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:tx_container.col4', 'colPos' => 104],
            ],
        ]
    )
    )
        ->setIcon('EXT:container/Resources/Public/Icons/container-4col.svg')
        ->setBackendTemplate('EXT:extended_bootstrap_package/Resources/Private/Backend/Templates/Container/Columns.html')
);
$GLOBALS['TCA']['tt_content']['types']['4_col']['showitem'] = $GLOBALS['TCA']['tt_content']['types']['2_col']['showitem'];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:extended_bootstrap_package/Configuration/FlexForms/Container/4Columns.xml',
    '4_col'
);

// Add new fields to tt_content
$additionalColumns = [
    'header_class' => [
        'l10n_mode' => 'prefixLangTitle',
        'label' => 'Header Class',
        'config' => [
            'type' => 'input',
            'size' => 10,
            'max' => 255,
        ],
    ],
    'subheader_class' => [
        'l10n_mode' => 'prefixLangTitle',
        'label' => 'Subheader Class',
        'config' => [
            'type' => 'input',
            'size' => 10,
            'max' => 255,
        ],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $additionalColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'headers',
    'header_class',
    'after: header_layout'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'headers',
    'subheader_class',
    'after: subheader'
);
