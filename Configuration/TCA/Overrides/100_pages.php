<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3_MODE') || die();

// Temporary variables
$extensionKey = 'extended_bootstrap_package';

// Register fields
$GLOBALS['TCA']['pages']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['pages']['columns'],
    [
        'is_megamenu' => [
            'exclude' => true,
            'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:pages.is_megamenu',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => 0,
                'suggestOptions' => [
                    'default' => [
                        'additionalSearchFields' => 'nav_title, alias, url',
                        'addWhere' => ' AND pages.uid != ###THIS_UID###',
                    ],
                ],
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'is_largedropdown' => [
            'exclude' => true,
            'label' => 'LLL:EXT:extended_bootstrap_package/Resources/Private/Language/locallang_be.xlf:pages.is_largedropdown',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                    ],
                ],
            ],
        ],
    ]
);
// Assign position to fields
ExtensionManagementUtility::addToAllTCAtypes('pages', 'is_largedropdown, is_megamenu', '1,4', 'after:nav_icon');
