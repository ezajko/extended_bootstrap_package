$(function() {
	if (window.matchMedia('(max-width: 991px)').matches) {
		$('#top-bar').removeClass('show');
	}
	if (window.matchMedia('(min-width: 992px)').matches) {
		$('#top-bar').addClass('show');
	}
});
$(window).resize(function() {
	if (window.matchMedia('(max-width: 991px)').matches) {
		$('#top-bar').removeClass('show');
	}
	if (window.matchMedia('(min-width: 992px)').matches) {
		$('#top-bar').addClass('show');
	}
});

// When the user scrolls the page, execute myFunction
['scroll', 'resize', 'DOMContentLoaded'].forEach(function(e) {
	window.addEventListener(e, myFunction);
});

// Get the navbar
var navbar = document.getElementById("page-header");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
	if (window.pageYOffset >= sticky) {
		navbar.classList.add("navbar-fixed-top")
	} else {
		navbar.classList.remove("navbar-fixed-top");
	}

}
