<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

// Make the extension configuration accessible
$extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
);
$bootstrapPackageConfiguration = $extensionConfiguration->get('bootstrap_package');

// PageTS
// Add Content Elements
if (!$bootstrapPackageConfiguration['disablePageTsContentElements']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:extended_bootstrap_package/Configuration/TsConfig/Page/ContentElement/All.tsconfig">');
}

// TCEFORM
if (!$bootstrapPackageConfiguration['disablePageTsTCEFORM']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:extended_bootstrap_package/Configuration/TsConfig/Page/TCEFORM.tsconfig">');
}
// Add default RTE configuration for extended bootstrap package
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bootstrap'] = 'EXT:extended_bootstrap_package/Configuration/RTE/Default.yaml';

// Register Icons
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

$icons = [
    'counter-group',
    'counter-group-item',
    'progress-bar-group',
    'progress-bar-group-item',
];
foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-extendedbootstrappackage-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:extended_bootstrap_package/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}
